# This file contains the implementation of the optimization

import numpy as np
from scipy.optimize import basinhopping  
from alflow_functions import running_alflow
import copy



def run_basinhopping(initial_parameters, experimental_data_list, fm, parameter_indices_to_optimize, experimental_input_var, parameter_bounds, n_iterations):
    fm_copy = copy.deepcopy(fm)
    def wrapped_objective_function(parameters):
        for i in range(len(parameters)):
            fm_copy[parameter_indices_to_optimize[i]] = parameters[i]

        total_error = 0.0
        for i in range(len(experimental_data_list)):
            experimental_data_i = experimental_data_list[i]
            model_output_i = running_alflow(fm_copy, experimental_input_var[i])[2]
            error_i = np.mean((model_output_i - experimental_data_i) ** 2)
            total_error += error_i
        print(total_error)

        
        return total_error

    bounds = tuple(parameter_bounds)
    minimizer_kwargs = {
        "method": "Nelder-Mead",
        "bounds": bounds,
    }
    
    
    result = basinhopping(wrapped_objective_function, initial_parameters, minimizer_kwargs=minimizer_kwargs, niter=n_iterations, stepsize=1, disp=True)
    
    optimized_parameters = result.x
    return optimized_parameters

