# This file contains functions that relates to Dash-functionality

import dash_bootstrap_components as dbc
from dash import  dcc, html, dash_table
import pandas as pd
import numpy as np
import plotly.graph_objs as go
from alflow_functions import expand_list
import base64
import io


def process_excel(file_contents):
    try:
        content_type, content_string = file_contents.split(',')
        decoded = base64.b64decode(content_string)
        df = pd.read_excel(io.BytesIO(decoded), sheet_name=None)
        data_strain = []
        data_stress = []
        data_strainrate = []
        data_temp = []
        data_taylor = []
        data_grain = []
        for sheet_name, sheet_data in df.items():
            if len(sheet_data.columns) >= 6:
                data_strain.extend([sheet_data.iloc[:, 0].tolist()])
                data_stress.extend([sheet_data.iloc[:, 1].tolist()])
                data_strainrate.extend([sheet_data.iloc[:, 2].tolist()])
                data_temp.extend([sheet_data.iloc[:, 3].tolist()])
                data_taylor.extend([sheet_data.iloc[:, 4].tolist()])
                data_grain.extend([sheet_data.iloc[:, 5].tolist()])
        data = [data_strain, data_stress, data_strainrate, data_temp, data_taylor, data_grain]
        return data
    except Exception as e:
        print(e)
        return None


def create_input_box(description, value, id_suffix):
    return html.Div([
        html.Div(description, style={'width': '60%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '15px'}),
        dbc.Input(value=value, type="number", invalid= (value == (float or int)) , id={'type': 'input-box','index': id_suffix}, style={"width": "40%", "display": "inline-block", 'font-family': 'Computer Modern', 'font-size': '15px'})
    ], style={"width": "100%", "margin-bottom": "5px", "margin-top": "5px"})


def create_all_variable_box(description, value,explained, id_suffix):
    return html.Div([
        html.Div(description, style={'width': '20%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '15px', 'text-align': 'center'}),
        dbc.Input(value=value, type="number", invalid= (value == (float or int)) , id={'type': 'all-variable-box','index': id_suffix}, style={"width": "28%", "display": "inline-block", 'font-family': 'Computer Modern', 'font-size': '15px',}),
        html.Div(explained, style={'width': '50%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '12px', 'marginLeft': '5px', 'padding-top': '20px'})
    ], style={"width": "100%", "margin-bottom": "10px", 'color':'light'})

def create_opt_variable_box(description, value,explained, id_suffix):
    return html.Div([
        dcc.Checklist(
            options=[{'label': '', 'value': 'check'}],  
            value=[],  # Initially unchecked
            id={'type': 'all-check-box', 'index': id_suffix},
            style={'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '15px'}
        ),        
        html.Div(description, style={'width': '20%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '15px', 'text-align': 'center'}),
        dbc.Input(value=value, type="number", invalid= (value == (float or int)) , id={'type': 'opt-variable-box','index': id_suffix}, style={"width": "28%", "display": "inline-block", 'font-family': 'Computer Modern', 'font-size': '15px',}),
        html.Div(explained, style={'width': '40%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '12px', 'marginLeft': '5px', 'padding-top': '20px'})
    ], style={"width": "100%", "margin-bottom": "10px", 'color':'light'})

def create_opt_ranges_box(description, value, index , id_suffix):
    return html.Div([dbc.Row([
        html.Div(description, style={'width': '15%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '15px', 'text-align': 'right'}),
        dbc.Input(value=value*0.8, type="number", invalid= (value == (float or int)) , id={'type': 'bottom-ranges-boxes','index': id_suffix}, style={"width": "15%", 'marginLeft': '30px', "display": "inline-block", 'font-family': 'Computer Modern', 'font-size': '15px',}),
        dbc.Input(value=value, type="number", invalid= (value == (float or int)) , id={'type': 'initial-guess-boxes','index': id_suffix}, style={"width": "15%",'marginLeft': '20px', "display": "inline-block", 'font-family': 'Computer Modern', 'font-size': '15px',}),
        dbc.Input(value=value*1.2, type="number", invalid= (value == (float or int)) , id={'type': 'top-ranges-boxes','index': id_suffix}, style={"width": "15%",'marginLeft': '20px', "display": "inline-block", 'font-family': 'Computer Modern', 'font-size': '15px',}),

        ])
        ], style={"width": "100%", "margin-bottom": "10px", 'color':'light'})


def create_datatable(input_list, input_desc, id_suffix):
    
    data_dict = {
        input_desc[i]: input_list[i]
        for i in range(len(input_desc))}
    df = pd.DataFrame(data_dict)

    return html.Div(
            dash_table.DataTable(
            data=df.to_dict('records'),
            columns=[{'id': c, 'name': c} for c in df.columns],
            
            id={'type': 'datatable','index': id_suffix},
            style_data={
                'color': 'black',
                'backgroundColor': 'white',
                'textAlign': 'center',
                'font-family': 'Computer Modern'
            },
            style_data_conditional=[
                {
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(220, 220, 220)',
                }
            ],
            style_header={
                'backgroundColor': 'rgb(210, 210, 210)',
                'color': 'black',
                'fontWeight': 'bold',
                'textAlign': 'center',
                'font-family': 'Computer Modern'
            },
            editable=True
        )
    )

    
def create_table_plot(input_alflow_list, input_desc):
    fig = go.Figure()
    
    fig.add_trace(go.Scatter(
        x=input_alflow_list[0],
        y=input_alflow_list[1],
        name=input_desc[1]
    ))
    
    
    fig.add_trace(go.Scatter(
        x=input_alflow_list[0],
        y=input_alflow_list[2],
        name=input_desc[2],
        yaxis="y2"
    ))
    
    fig.add_trace(go.Scatter(
        x=input_alflow_list[0],
        y=input_alflow_list[3],
        name=input_desc[3],
        yaxis="y3"
    ))

    fig.add_trace(go.Scatter(
        x=input_alflow_list[0],
        y=input_alflow_list[4],
        name=input_desc[4],
        yaxis="y4" 
    ))
    
    
    # Create axis objects
    fig.update_layout(
        #xaxis=dict(tickformat='.2e'),
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=50, 
            b=10, 
            l=10, 
            r=10,
        ),
        
        xaxis=dict(
            domain=[0.2, 0.8]
        ),
        yaxis=dict(
            range=[min(input_alflow_list[1])*0.95, max(input_alflow_list[1])*1.10],
            title=input_desc[1],
            color = 'darkblue'
        ),
        yaxis2=dict(
            range=[min(input_alflow_list[2])*0.90, max(input_alflow_list[2])*1.05],
            title=input_desc[2],
            color = 'red',
            anchor="free",
            overlaying="y",
            side="left",
            position=0.1
        ),
        yaxis3=dict(
            range=[min(input_alflow_list[3])*0.90, max(input_alflow_list[3])*1.10],
            title=input_desc[3],
            color = 'LightSeaGreen',
            anchor="x",
            overlaying="y",
            side="right"
        ),
        yaxis4=dict(
            range=[min(input_alflow_list[4])*0.95, max(input_alflow_list[4])*1.15],
            title=input_desc[4],
            color = 'blueviolet',
            anchor="free",
            overlaying="y",
            side="right",
            position=0.9
        )
    )
    
    return fig



def create_plot_data(result_data, data):
    micro_delta = result_data['delta']*1e6
    phi_degrees = result_data['phi']*180/np.pi
    m_taylor_array = np.array(data["input_list_alflow"][3])
    sigma_array = np.array(result_data['sigmaMPa'])
    tau_mega = sigma_array[1:1500]/m_taylor_array[1:1500]
    plot_e_x = tau_mega.tolist()
    plot_e_y = result_data['dsigma_deps'][1:1500]

    if data['plot'] is None: 
        plot_a = go.Scatter(x=result_data['strain'], y=result_data['rho_i'], name = "Plot 1")
        plot_b = go.Scatter(x=result_data['strain'], y=micro_delta, name = "Plot 1")
        plot_c = go.Scatter(x=result_data['strain'], y=phi_degrees, name = "Plot 1")
        plot_d = go.Scatter(x=result_data['strain'], y=result_data['sigmaMPa'], name = "Plot 1")
        plot_e = go.Scatter(x=plot_e_x, y=plot_e_y, name = "Plot 1")
        
        
        new_dict = {
            "plot-a": plot_a,
            "plot-b": plot_b,
            "plot-c": plot_c,
            "plot-d": plot_d,
            "plot-e": plot_e
            
        }
        
    else:
        old_dict = data['plot']
        index = len(old_dict.get("plot-a1", [])) + 2
        new_trace_a = go.Scatter(x=result_data['strain'], y=result_data['rho_i'], name = f"Plot {index}")
        new_trace_b = go.Scatter(x=result_data['strain'], y=micro_delta, name = f"Plot {index}")
        new_trace_c = go.Scatter(x=result_data['strain'], y=phi_degrees, name = f"Plot {index}")
        new_trace_d = go.Scatter(x=result_data['strain'], y=result_data['sigmaMPa'], name = f"Plot {index}")
        new_trace_e = go.Scatter(x=plot_e_x, y=plot_e_y, name = f"Plot {index}")
        
        if "plot-a1" not in old_dict:
            old_dict["plot-a1"] = [new_trace_a]
            old_dict["plot-b1"] = [new_trace_b]
            old_dict["plot-c1"] = [new_trace_c]
            old_dict["plot-d1"] = [new_trace_d]
            old_dict["plot-e1"] = [new_trace_e]
        else:
            old_dict["plot-a1"].append(new_trace_a)
            old_dict["plot-b1"].append(new_trace_b)
            old_dict["plot-c1"].append(new_trace_c)
            old_dict["plot-d1"].append(new_trace_d)
            old_dict["plot-e1"].append(new_trace_e)
        new_dict = old_dict
    
    data['plot'] = new_dict
    
    return data['plot']




def create_plots_from_plot_data(plot_data):
    plot_a = go.Figure(data=plot_data["plot-a"])
    plot_a.update_layout(
        title={'text': 'Dislocation density inside cells/subgrains' , 'x': 0.5},
        xaxis_title='strain   \u03b5',
        yaxis_title='density   \u03c1\u1d62  [m\u207b\u00b2]',
        yaxis=dict(tickformat='.1e', automargin = True),
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=30, 
            b=10, 
            l=10, 
            r=10, 
        )
    )

    plot_b = go.Figure(data=plot_data["plot-b"])
    plot_b.update_layout(
        title={'text': 'Average subgrain size', 'x': 0.5},
        xaxis_title= 'strain   \u03b5',
        yaxis_title='subgrain diameter   \u03b4 [\u03bcm]',
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=30, 
            b=10, 
            l=10, 
            r=10, 
        )
    )

    plot_c = go.Figure(data=plot_data["plot-c"])
    plot_c.update_layout(
        title={'text': 'Misorientation' , 'x': 0.5},
        xaxis_title= 'strain   \u03b5',
        yaxis_title='misorientation   \u03c6 [\u00b0]',
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=30,
            b=10, 
            l=10, 
            r=10, 
        )
    )
    
    plot_d = go.Figure(data=plot_data["plot-d"])
    plot_d.update_layout(
        title={'text':'Flow stress' , 'x': 0.5},
        xaxis_title= 'strain   \u03b5',
        yaxis_title='stress   \u03c3 [MPa]',
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=30, 
            b=10, 
            l=10,
            r=10, 
        )
    )
    
    plot_e = go.Figure(data=plot_data["plot-e"])
    plot_e.update_layout(
        title={'text':'Strain hardening rate' , 'x': 0.5},
        xaxis_title= 'stress \u03c4',
        yaxis_title='rate \u03b8 [MPa]',
        xaxis = dict(rangemode = "tozero"),
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=30, 
            b=10,
            l=10, 
            r=10, 
        )
    )
    
    if "plot-a1" in plot_data:
        for value_a in plot_data["plot-a1"]:
            plot_a.add_trace(value_a)
        for value_b in plot_data["plot-b1"]:
            plot_b.add_trace(value_b)
        for value_c in plot_data["plot-c1"]:
            plot_c.add_trace(value_c)
        for value_d in plot_data["plot-d1"]:
            plot_d.add_trace(value_d)
        for value_e in plot_data["plot-e1"]:
            plot_e.add_trace(value_e)
            
            
        plot_a.update_layout(legend={"itemclick": "toggle", "itemsizing": "constant" , 'x': 0.65, 'y': 0.05})
        plot_b.update_layout(legend={"itemclick": "toggle", "itemsizing": "constant" , 'x': 0.65, 'y': 0.95})
        plot_c.update_layout(legend={"itemclick": "toggle", "itemsizing": "constant" , 'x': 0.65, 'y': 0.05})
        plot_d.update_layout(legend={"itemclick": "toggle", "itemsizing": "constant" , 'x': 0.65, 'y': 0.05})
        plot_e.update_layout(legend={"itemclick": "toggle", "itemsizing": "constant" , 'x': 0.65, 'y': 0.95})
        
    content = html.Div([
        dbc.Row([
            dbc.Col(dcc.Graph(figure=plot_d, style={'height': '300px'}), width=4),
            dbc.Col(dcc.Graph(figure=plot_a, style={'height': '300px'}), width=4),
            dbc.Col(dcc.Graph(figure=plot_b, style={'height': '300px'}), width=4),
            dbc.Col(dcc.Graph(figure=plot_c, style={'height': '300px'}), width=4),
            dbc.Col(dcc.Graph(figure=plot_e, style={'height': '300px'}), width=4)
            ])
    ],
        style={'margin-left': '0%', 'padding-top': '5px'}
    )
    return content


def update_variables(input_values, all_variable_values, datatables, data):
    updated_input_data = data['input_data'].copy()  
    updated_variables_data = data['fm'].copy()  
    updated_input_list_table = data['input_list_table'].copy()  

    for index, value in enumerate(input_values):
        updated_input_data[index] = float(value) 
        
    for index, value in enumerate(all_variable_values):
        updated_variables_data[index] = float(value)  
    
    updated_input_list_table = data['input_list_table'] 
    for index, values in enumerate(datatables):
        table_dics = datatables[index]
        keys = table_dics[0].keys()        
        result = {key: [] for key in keys}

        for dic in table_dics:
            for key in dic:
                result[key].append(float(dic[key]))
    
            
        updated_input_list_table = list(result.values())
    
    updated_input_list_alflow = [expand_list(updated_input_list_table[i],1501) for i in range(0,5)]
    updated_data = data
    updated_data['datatable'] = create_datatable(updated_input_list_table,data['input_desc'],0)
    updated_data['input_data'] = updated_input_data
    updated_data['fm'] = updated_variables_data
    updated_data['input_list_table'] = updated_input_list_table
    updated_data['input_list_alflow'] = updated_input_list_alflow

    return updated_data

def create_comparison_plot(exp_strain,exp_stress, mod_stress):
    
    color_sequence = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
    exp_color = color_sequence[0]
    mod_color = exp_color 
    exp_width = 1
    mod_width = 3
    
    exp1 = go.Scatter(x=exp_strain[0], y=exp_stress[0], name = "Experimental 1", line=dict(color=exp_color, width=exp_width))
    mod = go.Scatter(x=exp_strain[0], y=mod_stress[0], name = "Modelled 1", line=dict(color=mod_color, width= mod_width, dash = 'dash'))
    plot_data = [mod]
    if len(exp_strain) > 1:
        for i in range(1,len(exp_strain)):
            exp_color = color_sequence[i]  
            mod_color = exp_color 

            
            n = i+1
            exp = go.Scatter(x=exp_strain[i], y=exp_stress[i], name = f"Experimental {n}", line=dict(color=exp_color, width=exp_width))
            mod = go.Scatter(x=exp_strain[i], y=mod_stress[i], name = f"Modelled {n}", line=dict(color=mod_color, width=mod_width, dash = 'dash'))
            plot_data.append(exp)
            plot_data.append(mod)
    comp_plot = go.Figure(data=exp1)
    comp_plot.update_layout(
        title={'text':'Flow stress' , 'x': 0.5},
        xaxis_title= 'strain   \u03b5',
        yaxis_title='stress   \u03c3 [MPa]',

        
        font=dict(family='Computer Modern', size=14),
        margin=dict(
            t=30, 
            b=10, 
            l=10, 
            r=0, 
        )
    )
    for value in plot_data:
        comp_plot.add_trace(value)
    
    comp_plot.update_layout(legend={"itemclick": "toggle", "itemsizing": "constant" , 'x': 1.0, 'y': 1.0})
        
    content = html.Div([
        dbc.Row([
            dbc.Col(dcc.Graph(figure=comp_plot, style={'height': '600px'}), width=12)
            ])
    ],
        style={'margin-left': '0%', 'padding-top': '5px'}
    )
    return content
