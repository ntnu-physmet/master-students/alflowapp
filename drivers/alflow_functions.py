# This file contains functions that handle the ALFLOW Extension Module, as well as other ALFLOW-related tasks

import alflow_v6                          # importing the fortran extension module
import shutil                             # for copying file
import numpy as np                        


# The function that use the Fortran Extension Module for ALFLOW-computations
def running_alflow(fm ,input_var):
    # fm : the 62 parameter list in the format [p0, p1, p2 , ... , p61]
    # input_var : a list of 5 lists, each containing 1501 data points for strain, strain-rate, temperature, grain size, taylor factor
    data = alflow_v6.alflow_v6(fm,input_var)
    return data


#FUNCTION initialize() returns a list containing all contents from the alloy-file "input_datafile"
def initialize_input(input_datafile):
    i_df = open(input_datafile, "r")
    input_filedata = i_df.read() # read datafile into a list of strings, each string is a row containing one data variable
    input_filelines = input_filedata.splitlines()
    i_df.close()
    
    list_elements = 62  # len(file_lines) , but use fixed values for now
    fm = [0]*list_elements  # list of variables
    fm_desc = [0]*list_elements
    fm_explained = [0]*list_elements
    
    for i in range(list_elements):
        # the files use these spots for the variables
        float_string = input_filelines[i][0:16].strip()
        fm[i] = float(float_string)
        fm_desc_string = input_filelines[i][20:34].strip()
        fm_desc[i] = fm_desc_string
        fm_explained_string = input_filelines[i][34:].strip()
        fm_explained[i] = fm_explained_string

        
    return fm, fm_desc, fm_explained

# Function used to set standard values for these strain-conditions
def initialize_table():
    t0_in = 293                # Temperature [K]
    gamdot0_in = 0.01          # strain rate dEps/dt [1/s] 
    strain_end = 5             # initial concentration
    d0_um = 50                 # grain size in micrometer, corrected later
    m_taylor_in = 3            # Taylor factor
    input_values = [strain_end, t0_in , gamdot0_in , m_taylor_in,  d0_um]
    input_desc = ['Strain [MPa]' , 'Temperature [K]' , 'Strainrate [1/s]' , 'Taylor factor (M)', 'Grain size [uM]']
    return input_values, input_desc


# Function used to expand a list and make linear regressions at value flips
def expand_list(old_list, new_length):
    new_length = new_length-1
    new_list = []
    #checking if all values are the same
    if old_list.count(old_list[0])==len(old_list):
        new_list = [old_list[0]]*(new_length+1)
        
    else:
        full_list = np.array([])
        for i in range(len(old_list)-1):
            segment = np.linspace(old_list[i], old_list[i+1], new_length+1)
            segment = segment[0:-1:]
            full_list = np.concatenate((full_list, segment))
            
        new_list = full_list[0::len(old_list)-1]
        new_list = np.append(new_list,old_list[-1])
    
    return list(new_list)


# Function that replaces the alloy-file that is shown
def use_datafile(filename, input_datafile):
    input_filepath = "./data/" + str(filename)
    if input_filepath: 
        shutil.copyfile(input_filepath, input_datafile)
    return         
        


