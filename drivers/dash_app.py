# This file contains the Dash app

import dash
import dash_bootstrap_components as dbc
from dash import Input, Output, dcc, html, State
from dash.dependencies import ALL
import pandas as pd
import os
from alflow_functions import running_alflow, initialize_table, initialize_input, expand_list, use_datafile
from dash_functions import process_excel, create_all_variable_box, create_opt_variable_box, create_opt_ranges_box, create_input_box, create_datatable, create_table_plot, create_plot_data, create_plots_from_plot_data, update_variables, create_comparison_plot
from optimization import run_basinhopping
import time

input_datafile = ("./inputdata")

#  initializing stored values
fm, fm_desc, fm_explained = initialize_input(input_datafile)
input_data, input_desc = initialize_table()

input_list_table = [expand_list([input_data[i]],11) for i in range(1,5)]
input_list_table = [expand_list([0,input_data[0]],11)] + input_list_table
input_list_alflow = [expand_list(input_list_table[i],1501) for i in range(0,5)]

editable_table = create_datatable(input_list_table,input_desc,0)

all_variable_boxes = [create_all_variable_box(description, value, explained , i) for i, (description, value,explained) in enumerate(zip(fm_desc, fm , fm_explained))]
input_boxes = [create_input_box(description, value, i) for i, (description, value) in enumerate(zip(input_desc, input_data))]

data_dict = {
    'input_data': input_data,
    'input_desc': input_desc,
    'input_list_table': input_list_table,
    'input_list_alflow': input_list_alflow,
    'fm': fm,
    'fm_desc': fm_desc,
    'fm_explained': fm_explained,
    'datatable': editable_table,
    'plot' : None,
    'alflow_results' : None,
    'optimization_parameters': None,
    'experimental_data': None
    }


          
app = dash.Dash(external_stylesheets=[dbc.themes.BOOTSTRAP])

app.layout = dbc.Container(
    [
        dcc.Store(id="store_data", data = data_dict),
        dbc.Row(
            [
                dbc.Col(
                    dbc.Nav(
                        [
                            dbc.NavLink("Run Alflow", href="#", id="run-alflow-button", style={'color': 'blue'}),
                            html.Div(style={'height': '20px'}),  # Empty div for spacing
                            dbc.NavLink("Download results", href="#", id="btn-download-txt", style={'color': 'black'}),
                            dbc.NavLink("Reset results", href="#", id="reset-alflow-button", style={'color': 'red'}),
                            dcc.Download(id="download-text"),
                        ],
                        vertical=True,
                        pills=True,
                    ),
                    width=2,
                    className="bg-light",
                    style={'position': 'fixed', 'top': 0, 'left': 0, 'height': '100vh', 'padding': '40px', 'width':'30vh', 'font-size': '18px'},
                ),
                dbc.Col(
                    dbc.Tabs(
                        [                              
                            dbc.Tab(label="Plots", tab_id="plot"),
                            dbc.Tab([
                                dbc.Row([ 
                                    dbc.Col([
                                        dbc.Button(
                                            "Update parameters", id="update-variable-button-table", outline=True, color="primary", className="me-1"
                                        ),
                                    ], width=3),
                                    dbc.Col([
                                        dbc.Button(
                                            "Apply values to table", id="reset-parameters-button", outline=True, color="secondary", className="me-1"
                                            ),
                                        ], width=3),
                                ], style={'margin-left': '3%', 'padding-top': '30px', 'font-size': '18px'}),
                                ] , label="Table", tab_id="table"),
                            dbc.Tab([
                                dbc.Row([ 
                                    dbc.Col([
                                        dbc.Button(
                                            "Update parameters", id="update-variable-button-input", outline=True, color="primary", className="me-1"
                                        ),
                                    ], width=3),
                                    dbc.Col([
                                        dcc.Dropdown(
                                            id='file-dropdown',
                                            options=[
                                                {'label': file, 'value': file} for file in os.listdir('./data')],
                                            placeholder='Select a file...')
                                    ], width=3),
                                    dbc.Col([
                                        dbc.Button(
                                            "Use selected file", id="use-datafile-button", outline=True, color="secondary", className="me-1"
                                        ),
                                    ], width=2),
                                    
                                ],
                                    style={'margin-left': '3%', 'padding-top': '30px', 'font-size': '18px'}
                                ),
                            ], label="Input", tab_id="input"),
                            dbc.Tab([
                                dbc.Row([
                                    dbc.Col([
                                        dcc.Upload(
                                            id='upload-data',
                                            children=dbc.Button(
                                                        'Drag or click to upload experimental data'
                                                    , outline=True, color="secondary", className="me-1"
                                            ))
                                    ], width=3),
                                    dbc.Col([
                                        dbc.Input(id='iterations-input', placeholder="Iterations (1-10)", type='number'),
                                    ], width=3),
                                    dbc.Col([
                                        dbc.Button(
                                            "Start optimization", id="optimize-button", outline=True, color="primary", className="me-1"
                                        )
                                    ], width=3),
                                ],
                                    style={'margin-left': '3%', 'padding-top': '30px', 'font-size': '18px'}
                                ),
                                dbc.Row(dbc.Col(html.Div(id='output-optimized-parameters'), width = 12))
                            ], label="Optimize", tab_id="optimization"),
                            dbc.Tab([
                                dbc.Row([ 
                                    dbc.Col([
                                        dbc.Button(
                                            "Save parameters to optimize", id="save-parameters-button", outline=True, color="primary", className="me-1"
                                        ),
                                    ], width=3),
                                    dbc.Col([
                                        dcc.Dropdown(
                                            id='file-dropdown-opt',
                                            options=[
                                                {'label': file, 'value': file} for file in os.listdir('./data')],
                                            placeholder='Select a file...')
                                    ], width=3),
                                    dbc.Col([
                                        dbc.Button(
                                            "Use selected file", id="use-opt-datafile-button", outline=True, color="secondary", className="me-1"
                                        ),
                                    ], width=2)
                                    
                                ],
                                    style={'margin-left': '3%', 'padding-top': '30px', 'font-size': '18px'}
                                ),
                            ], label="Parameters for optimization", tab_id="optimization-parameters"),
                        ],
                        id="tabs",
                        active_tab="plot",
                    ),
                    width=10,
                    style={'margin-left': '10%', 'padding-top': '15px'},
                ),
            ]
        ),
        html.Div(id="tab-content", className="p-4", style={'margin-left': '10%', 'padding-top': '20px'}),
    ]
)



@app.callback(
    Output("tab-content", "children"),
    [Input("tabs", "active_tab"), Input("store_data", "data")],
)
def render_tab_content(active_tab,  data):
    plot_data = data['plot']
    if active_tab == "plot" and plot_data is not None:
        
        content = create_plots_from_plot_data(plot_data)
        return content
        
    elif active_tab == "table":
        input_var = data['input_data']
        input_desc = data['input_desc']
        input_list_table = data['input_list_table']
        input_alflow_list = data['input_list_alflow']

        input_boxes = [create_input_box(description, value, i) for i, (description, value) in enumerate(zip(input_desc, input_var))]

        editable_table = create_datatable(input_list_table, input_desc, 0)
        table_plot = create_table_plot(input_alflow_list, input_desc)  
        
        content = html.Div([
            
            dbc.Row([
                dbc.Col([
                    dbc.Row(html.Div(input_boxes))
                    ], width = 3),

                dbc.Col(html.Div(editable_table), width = 7),
            ]),
            dbc.Row(dcc.Graph(figure=table_plot))  
        ])
        return content
    
    elif active_tab == "input":
        
        all_variables = data['fm']
        all_variables_descriptions = data['fm_desc']
        all_variables_explained = data['fm_explained']
        
        all_variable_boxes = [create_all_variable_box(description, value, explained , i) for i, (description, value,explained) in enumerate(zip(all_variables_descriptions, all_variables , all_variables_explained))]

        content = html.Div([
            dbc.Row([
                dbc.Col(html.Div(all_variable_boxes[0:32]), width = 5),
                dbc.Col(html.Div(all_variable_boxes[32:]), width = 5, style={'margin-left': '10%'})
            ])
        ])
        return content
    
    elif active_tab == "optimization":
        if data["optimization_parameters"] is None:
            content =  "Select optimzation parameters in tab to the right"
        else:
            fm_desc = data['fm_desc']
            param_data = data["optimization_parameters"]
            chosen_variables_index, chosen_variables = param_data[0], param_data[1]
            chosen_variables_descriptions = [fm_desc[int(index)] for index in chosen_variables_index]
            optimization_boxes = [create_opt_ranges_box(description, value, index , i) for i, (description, value, index) in enumerate(zip(chosen_variables_descriptions, chosen_variables , chosen_variables_index))]
            
            content = html.Div([
                html.Div([dbc.Row([
                    html.Div("Parameter", style={'width': '15%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '20px', 'text-align': 'right'}),
                    html.Div("Bottom range", style={'width': '15%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '20px', 'text-align': 'center'}),
                    html.Div("Initial guess", style={'width': '15%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '20px', 'text-align': 'center'}),
                    html.Div("Top range", style={'width': '15%', 'display': 'inline-block', 'font-family': 'Computer Modern', 'font-size': '20px', 'text-align': 'center'})    
                    ])
                    ], style={"width": "100%", "margin-bottom": "10px", 'color':'light'}),
                html.Div([dbc.Row([
                    dbc.Col(html.Div(optimization_boxes))
                    ])
                    ], style={"width": "100%", "margin-bottom": "10px", 'color':'light'})
                ])
            
        return content
            
        
    elif active_tab == "optimization-parameters":
        all_variables = data['fm']
        all_variables_descriptions = data['fm_desc']
        all_variables_explained = data['fm_explained']
        
        opt_variable_boxes = [create_opt_variable_box(description, value, explained , i) for i, (description, value,explained) in enumerate(zip(all_variables_descriptions, all_variables , all_variables_explained))]

        content = html.Div([
            dbc.Row([
                dbc.Col(html.Div(opt_variable_boxes[0:32]), width = 5),
                dbc.Col(html.Div(opt_variable_boxes[32:]), width = 5, style={'margin-left': '10%'})
            ])
        ])
        
        return content

    return "Press Run Alflow for results"

@app.callback(
    Output("download-text", "data"),
    [Input("btn-download-txt", "n_clicks"), State("store_data", "data")],
    prevent_initial_call=True
)
def download_file(n_clicks, data):
    
    if n_clicks and data['alflow_results'] is not None:
        writer = pd.ExcelWriter("AlflowData.xlsx", engine='xlsxwriter')
        
        for i in range(len(data['alflow_results'])):
            df = pd.DataFrame(data['alflow_results'][i])  
            sheet_name = f'Plot{i+1}'
            df.to_excel(writer, sheet_name=sheet_name)
        writer.close()
        return dcc.send_file("AlflowData.xlsx")

    return dash.no_update


@app.callback(
    Output("store_data", "data"),
    [State({"type": "input-box", "index": ALL}, "value"),
    State({"type": "all-variable-box", "index": ALL}, "value"),
    State({"type": "datatable", "index": ALL}, "data"),
    Input("run-alflow-button", "n_clicks"),
    Input("update-variable-button-table", "n_clicks"),
    Input("update-variable-button-input", "n_clicks"),
    Input("use-datafile-button", "n_clicks"),
    Input("reset-parameters-button", "n_clicks"),
    Input("reset-alflow-button", "n_clicks"),
    State("store_data", "data"),
    State('file-dropdown', 'value'),
    State('file-dropdown-opt', 'value'),
    Input("use-opt-datafile-button", "n_clicks"),
    State({"type": "opt-variable-box", "index": ALL}, "value"),
    State({'type': 'all-check-box', "index": ALL}, "value"),
    Input("save-parameters-button", "n_clicks"),
    Input('upload-data', 'contents')
    ],
)


def update_input_values(input_values, all_variable_values, datatables, n_clicks_rund_alflow , n_clicks_var_tab, n_clicks_var_inp, n_clicks_dropdown, n_clicks_reset , n_clicks_reset_plot, data, selected_file, selected_opt_file, n_clicks_dropdown_opt, all_opt_variables, all_checkboxes, n_clicks_save, excel_contents):
    ctx = dash.callback_context
    if not ctx.triggered:
        return dash.no_update

    triggered_id = ctx.triggered[0]['prop_id'].split('.')[0]
    
    if triggered_id == 'run-alflow-button':
        
        input_list_alflow = data['input_list_alflow']
        fm = data['fm']
        L = running_alflow(fm,[input_list_alflow[1],input_list_alflow[2], input_list_alflow[0],input_list_alflow[3], input_list_alflow[4]])
        L_df = pd.DataFrame(L)
        N_df = L_df.T

        result_data = {
            'sigma_a': N_df[0],
            'strain': N_df[1],
            'sigmaMPa': N_df[2],
            'rho_i': N_df[3],
            'delta': N_df[4],
            'rho': N_df[5],
            'phi': N_df[6],
            'strainrate': N_df[7],
            'conc_sc': N_df[8],
            'sigma_Pa': N_df[9],
            'tau_t': N_df[10],
            'dsigma_deps': N_df[11],
            'temperature': input_list_alflow[1][0:-1],
            'taylor': input_list_alflow[3][0:-1],
            'grain size': input_list_alflow[4][0:-1]
            
        }
        if data['alflow_results'] is None:
            data['alflow_results'] = [result_data]
        else: 
            
            result_list = data['alflow_results']
            result_list.append(result_data)
            data['alflow_results'] = result_list
        
        
        plot_data = create_plot_data(result_data, data)
        data['plot'] = plot_data
        
        return data
    
    elif triggered_id == 'reset-alflow-button':
        
        data['plot'] = None
        data['alflow_results'] = None
        return data
    
    
    
    elif triggered_id == 'update-variable-button-table' or triggered_id == 'update-variable-button-input':
        updated_data = update_variables(input_values, all_variable_values, datatables, data)

        return updated_data
    
    
    elif triggered_id == "use-datafile-button":
        if selected_file != None:
            use_datafile(selected_file, input_datafile)
            fm, fm_desc, fm_explained = initialize_input(input_datafile)
            data['fm'] = fm
            data['fm_desc'] = fm_desc
            data['fm_explained'] = fm_explained
            
            return data
        
    elif triggered_id == "use-datafile-button-opt":
        if selected_opt_file != None:
            use_datafile(selected_opt_file, input_datafile)
            fm, fm_desc, fm_explained = initialize_input(input_datafile)
            data['fm'] = fm
            data['fm_desc'] = fm_desc
            data['fm_explained'] = fm_explained
            
            return data
    
    
    
    elif triggered_id == 'reset-parameters-button':
        
        input_data = data['input_data'].copy()
        
        for index, value in enumerate(input_values):
            input_data[index] = float(value) 
        
        input_list_table = [expand_list([input_data[i]],11) for i in range(1,5)]
        input_list_table = [expand_list([0,input_data[0]],11)] + input_list_table
        
        data['input_list_table'] = input_list_table
        data['input_data'] = input_data
        
        return data
    
    elif triggered_id == 'save-parameters-button':
        optimization_data = [[],[],[]]
        optimization_data[2] = all_opt_variables
        for i in range(len(all_checkboxes)):
            if all_checkboxes[i] == ["check"]:
                optimization_data[0].append(i)
                optimization_data[1].append(all_opt_variables[i])

        data['optimization_parameters'] = optimization_data
                
        return data
    
    elif triggered_id == 'upload-data' :
        if excel_contents is not None:
            excel_data = process_excel(excel_contents)
            data['experimental_data'] = excel_data   
        return data
                
        

    return dash.no_update

# Callback to run optimization when the button is clicked
@app.callback(
    Output('output-optimized-parameters', 'children'),
    [Input('optimize-button', 'n_clicks'),
    State("store_data", "data"),
    State('iterations-input', 'value'),
    State({"type": "bottom-ranges-boxes", "index": ALL}, "value"),
    State({"type": "initial-guess-boxes", "index": ALL}, "value"),
    State({"type": "top-ranges-boxes", "index": ALL}, "value")]
)
def optimize_parameters(n_clicks, data, n_iterations, bottom_ranges, initial_guess, top_ranges):
    if n_clicks:
        experimental_data = data["experimental_data"]
        opt_param = data['optimization_parameters']
        
        initial_parameters = [guess for guess in initial_guess]
        parameter_indices_to_optimize = [index for index in opt_param[0]]
        parameter_bounds = [tuple([bottom_ranges[i],top_ranges[i]]) for i in range(len(top_ranges))]
        fm = opt_param[2]
        exp_strain = experimental_data[0]
        exp_stress = experimental_data[1]
        exp_strainrate = experimental_data[2]
        exp_temp = experimental_data[3]
        exp_taylor = experimental_data[4]
        exp_grain = experimental_data[5]

        
        input_vars = [0]*len(exp_stress)
        for n in range(len(exp_stress)):
            exp_temp[n].append(exp_temp[n][-1])  
            exp_strainrate[n].append(exp_strainrate[n][-1])  
            exp_strain[n].append(exp_strain[n][-1])  
            exp_taylor[n].append(exp_taylor[n][-1])  
            exp_grain[n].append(exp_grain[n][-1])  
            input_vars[n] = [exp_temp[n], exp_strainrate[n], exp_strain[n], exp_taylor[n], exp_grain[n]]

        experimental_input_var = input_vars
        experimental_data_list = exp_stress
        start_time = time.time()
        # Run basinhopping optimization
        optimized_params = run_basinhopping(initial_parameters, experimental_data_list, fm, parameter_indices_to_optimize, experimental_input_var,parameter_bounds,n_iterations)

        finish_time = (time.time() - start_time)
        finish_time = "{:.1f}".format(finish_time)
        
        for i in range(len(parameter_indices_to_optimize)):
            index = parameter_indices_to_optimize[i]
            fm[int(index)] = optimized_params[i]
        
        modelled_stress = [0]*len(exp_strain)
        for i in range(len(exp_strain)):
            modelled_stress[i] = running_alflow(fm, experimental_input_var[i])[2]
        
        plot_data = create_comparison_plot(exp_strain,exp_stress, modelled_stress)
        
        content = html.Div([
            html.Div(f'Optimized Parameters: {optimized_params} found in {finish_time} seconds', style={'margin-left': '10%', 'padding-top': '20px'}),
            plot_data
            ])
        
    
        return content
    return ''  # Initial or empty state




if __name__ == "__main__":
    app.title = "ALFLOW"
    #app.run_server(debug = True, port=2526) #for local testing
    port = os.environ.get('dash_port')
    debug = os.environ.get('dash_debug')=="True"
    app.run_server(debug = debug, host="0.0.0.0", port=port)
