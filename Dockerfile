# Use an official Python runtime as a parent image
FROM python:3.10-slim

# Create a directory where the code is to be hosted
RUN mkdir /app

# Set the working directory inside the container
WORKDIR /app

# Copy the requirements file into the container
COPY requirements.txt /app/requirements.txt



# Install any needed packages specified in requirements.txt
RUN apt-get update \
    && apt-get install -y libpq-dev gcc \
    && apt-get install libgfortran5 \
    && pip install --no-cache-dir -r requirements.txt

COPY /drivers/ /app/
COPY /basic/ /app/






# Define environment variables
ENV PYTHONPATH /app/drivers

ENV dash_port=80
ENV dash_debug="False"


# Specify the command to run your Dash app
CMD ["python", "dash_app.py"]