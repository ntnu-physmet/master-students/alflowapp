c----------------------------------------------------------------------
      Subroutine conc_af(conc_sc,rho_m, T, gamdot)
c----------------------------------------------------------------------

c  -- DSA model for effective solute (Mg) in the Cotrell cloud 
c  -- surrounding the dislocation core

      implicit none

c  -- transfered
      double precision conc_sc,rho_m, T, gamdot

c  -- include common blocks and their declarations
      include "cmnprm.f"

c  -- local
      double precision fun, conc_ss, arg

      conc_ss  = conc*exp(dU_sc/(k*T))
      If (gamdot .gt. 0.) then
         arg = B_sc* b**2* nu_Debye* rho_m* exp(-U_sd/(k*T))/gamdot
         fun = 1. - exp(-arg**n_sc)
         conc_sc  = conc_0 + conc + fun*(conc_ss-conc)
      else
         conc_sc  = conc_0 + conc_ss
      endif

      return
      end


c----------------------------------------------------------------------
      Subroutine G_subaf(G,T)
c----------------------------------------------------------------------

c     calculate the shear module as a function of temperature.

      implicit none
      double precision G,T

      G  = 2.99E10 *exp(-5.4E-4*T)

      return
      end




