      subroutine eqs_gam(NEQ, gamma, Y, dY, RPAR, IPAR)

c     Calculate the derivatives of rho, delta and phi
c     with respect to gamma.

c --  Decide which kind of recovery is allowed?
c --  binary system: RC_mode = xyz, where xyz are digits
c --  equal to 0 if false or 1 if true. 
c --  x = pure metal climb (pm_c)
c --  y = solute glide     (s_g )
c --  z = solute climb     (s_c )
c
c     RC_mode=7: pm_c + s_g + s_c
c     RC_mode=6: pm_c + s_g
c     RC_mode=5: pm_c       + s_c
c     RC_mode=4: pm_c
c     RC_mode=3:        s_g + s_c
c     RC_mode=2:        s_g
c     RC_mode=1:              s_c
c
c     note: only RC_mode 2=001 or 4=010 are implemented so far!
c           the other options will do nonsens!!
      implicit none

      Integer NEQ, Ipar
      DOUBLE PRECISION Y(Neq), dY(Neq), gamma, RPAR(*)

c  -- local

      double precision ebar,pi
      parameter(ebar = 2.718281828, pi=3.141592654)

      double precision gamma0,gamma1,T0,T1,gamdot0,gamdot1,D0,D1
      double precision fac0, fac1
      double precision L_prec_m1, L_disp_m1, L_prim_m1
      double precision rho, L_rho, L_D, L_eff
      double precision arg1, arg_pre, arg_exp, arg_sinh
      double precision dpluss, dminus
      double precision S, phi_II
      double precision drhoi, ddelta, dphi,rhoi, delta, phi
      double precision D, T, gamdot, G, delta_IV
      double precision nu_c_pm, nu_g_s, nu_c_s, nu_RC
      double precision nu_delta_pm, nu_delta_s
      double precision rho_m, conc_sc

      double precision K_II, fac, phi_4
c      double precision LG, delta_III, kappa_4,phi0
c      double precision LG,phi0

c  -- functions
      double precision exp2sinh

      include "cmnprm.f"

c---------------------------------------------------
c  -- rename the variables to make them recognizable.

      rhoi  = Y(1)
      delta = Y(2)
      phi   = Y(3)

c  -- transferred external variables, interpolate

      gamma0  = rpar(1)
      gamma1  = rpar(2)
      T0      = rpar(3)
      T1      = rpar(4)
      gamdot0 = rpar(5)
      gamdot1 = rpar(6) 
      D0      = rpar(7)
      D1      = rpar(8)

      fac1    = (gamma-gamma0)/(gamma1-gamma0)
      fac0    = 1.-fac1

      T       = T0*fac0 + T1*fac1
      gamdot  = gamdot0*fac0 + gamdot1*fac1
      D       = D0*fac0 + D1*fac1

      nu_c_pm     = 0.0
      nu_g_s      = 0.0
      nu_c_s      = 0.0
      nu_delta_pm = 0.0
      nu_delta_s  = 0.0
      
c------------------------------
c --- Calculate rho  ----------
c------------------------------

c  -- lagt til et delta0 ledd, som forsvinner hvis delta0 settes lik null
c  -- Jesper, 4/5 - 2006
c      phi0 = 3.0*pi/180.
c      rho = rhoi + kappa_0*phi/(b*delta) + kappa_0*phi0/(b*delta0)
      rho = rhoi + kappa_0*phi/(b*delta)

c----------------------------
c --- Calculate G  ----------
c----------------------------

      Call G_subAF(G,T)

c-----------------------------------
c --- local conc.  of Mg due to DSA 
c-----------------------------------

      rho_m = m*rho

c  NOTE only if solute, otherwice use conc and conc0 !

      if(RC_mode .NE. 4) call conc_AF(conc_sc,rho_m,T,gamdot)

c------------------------------
c --- the slip length ---------
c------------------------------

      L_rho = C/Sqrt(rho)

      if (r_prec.le.0.0) r_prec = 1.e5
      L_prec_m1 = (2.*f_prec*kappa3_prec)/r_prec

      if (r_disp.le.0.0) r_disp = 1.e5
      L_disp_m1 = (2.*f_disp*kappa3_disp)/r_disp

      if (r_prim.le.0.0) r_prim = 1.e5
      L_prim_m1 = (2.*f_prim*kappa3_prim)/r_prim

      if (D.le.0.0) D = 1.e5
      L_D   = D/kappa_2

      L_eff = 1./Sqrt(1./L_rho**2  + 1./L_D**2 + 
     $     L_prec_m1**2 + L_disp_m1**2 + L_prim_m1**2)


c-------------------------------
c --- Calculate drhoi/dgamma ---
c-------------------------------

c  -- first the production of dislocations to be stored

      arg1   = 2./(1.+f*(q_b**2-1.))
      dpluss = arg1/(b*L_eff)

c  -- now the recovery term, starting with jump frequencies
c  -- for three different cases:

c  -- case c_pm = "climb" and "pure metal":

      If(RC_mode .ge. 4) then
         arg_pre  = rhoi* B_rho_pm* ksi_rho_pm* nu_Debye* b**2
         arg_exp  = -U_SD/(k*T)
         arg_sinh = 2.* ksi_rho_pm* b**4* G* Sqrt(rhoi)/(k*T)
         nu_c_pm  = arg_pre* exp2sinh(arg_exp,arg_sinh)
      endif

c  -- case g_s = "glide" and "solute":

      If((RC_mode .eq. 2).or.(RC_mode .eq. 3).or.(RC_mode .eq. 6)
     &     .or.(RC_mode .eq. 7)) then
         arg_pre  = (Sqrt(rhoi)*B_rho_sg*ksi_rho_sg*b*nu_Debye)/
     &        (omega_s *conc_sc**(1.-e_rho))
         arg_exp  = -(U_s+dU_s)/(k*T)
         arg_sinh = conc_sc**(-e_rho)* ksi_rho_sg* omega_s* b**4*
     &        G* Sqrt(rhoi)/ (k*T*2.*pi)
         nu_g_s  = arg_pre* exp2sinh(arg_exp,arg_sinh)
      endif

c  -- case c_s = "climb" and "solute":

      If((RC_mode .eq. 1).or.(RC_mode .eq. 3).or.(RC_mode .eq. 5)
     &     .or.(RC_mode .eq. 7)) then
         arg_pre  = rhoi* B_rho_sc* ksi_rho_sc* nu_Debye* b**2/
     &        (conc_sc)
         arg_exp  = -(U_s+dU_s)/(k*T)
         arg_sinh = 2.* ksi_rho_sc* b**4* G* Sqrt(rhoi)/(k*T)
         nu_c_s   = arg_pre* exp2sinh(arg_exp,arg_sinh)
      endif

c --  see explaination in the header of this subroutine
      If     (RC_mode .eq. 7) then
         nu_RC = Min(nu_c_pm,nu_g_s,nu_c_s)
      ElseIf (RC_mode .eq. 6) then
         nu_RC = Min(nu_c_pm,nu_g_s)
      ElseIf (RC_mode .eq. 5) then
         nu_RC = Min(nu_c_pm,nu_c_s)
      ElseIf (RC_mode .eq. 4) then
         nu_RC = nu_c_pm
      ElseIf (RC_mode .eq. 3) then
         nu_RC = Min(nu_g_s,nu_c_s)
      ElseIf (RC_mode .eq. 2) then
         nu_RC = nu_g_s
      ElseIf (RC_mode .eq. 1) then
         nu_RC = nu_c_s
      Else 
         Write(*,*) "RC_mode = ", RC_mode," is not allowed"
         Write(*,*) "Please put RC_mode in the range 1-7"
         stop
      EndIf

c  -- Recovery term

      dminus = - rhoi* nu_RC/gamdot

c  -- drhoi/dgamma

      drhoi = dpluss + dminus

c------------------------------
c --- Calculate dphi/dgamma  --
c------------------------------

      K_II = (q_c*f*(q_b**2-1.))**2
      K_II = K_II/(1+f*(q_b**2-1.))
      K_II = K_II/kappa_0**2

c  -- Make dphi depend on D (Jesper Friis, 02.05.2006)
c      delta_III = 1e-6
c      kappa_4   = 1.5
c      LG = 1./(1./delta_III + kappa_4/D)

      phi_4 = phi_IV

c     fac is temporarily changed to the expression from icaa10
      fac = min(1.,(sqrt(rhoi)*delta/q_c)**15)
c      fac = 1.-min(1.,(-2.*dminus/dpluss)**5)

      dphi=0.02*(T/300.)**2
c      dphi = dphi * (delta_III/LG)**1

      dphi = (1.-fac)* dphi + fac* b* K_II/(phi* L_eff)  
      dphi = (1.-(phi/phi_4)**5)*dphi



c--------------------------------
c --- Calculate ddelta/dgamma ---
c--------------------------------

c  -- fictive II-III transition-values:

      phi_II   = b*q_c*f*(q_b**2-1.)*Sqrt(rhoi)/kappa_0

c  -- the parameter S (equal to S_II in stage II)

      if(phi .le. phi_II) then
         S = S_II
      Else
         S=S_II+(phi-phi_II)*(S_IV-S_II)/(phi_IV-phi_II)
      EndIf
      If (phi.ge.phi_IV) S = S_IV

c  -- first the decreases of delta due to production of stored 
c  -- dislocations building new boundaries:

      dminus   = -2.0* S* delta**2* rhoi* L_rho**2/ 
     &     (L_eff* kappa_0* phi)
c
c     Eriks version:
c      dminus = -2.0* S* delta**2* rhoi/(kappa_0* phi)
c      dminus = dminus*(fac*L_rho**2/L_eff +(1.-fac)*L_eff) 

c  -- now the recovery term, starting with jump frequencies
c  -- for two different cases:

c  -- case pm =  "pure metal":

      If(RC_mode .ge. 4) then
         arg_pre  = B_delta_pm* b* nu_Debye
         arg_exp  = - U_SD/(k*T)
         arg_sinh = ksi_delta_pm* b**4* phi* G* log(ebar*phi_c/phi)
     &        /(k* T* delta* (1-nu)* pi)
         nu_delta_pm = arg_pre* exp2sinh(arg_exp,arg_sinh)
      endif

c  -- case s =  "solute":

      If(RC_mode .ne. 4) then
         arg_pre  = B_delta_s* b* nu_Debye
         arg_exp  = -(U_s+dU_s)/(k*T)
         arg_sinh = ksi_delta_s* b**4* phi* G* log(ebar*phi_c/phi)
     &        /(k* T* delta* (1-nu)* pi* conc_sc**e_delta)
         nu_delta_s = arg_pre* exp2sinh(arg_exp,arg_sinh)
      endif

c --  see explaination in the header of this subroutine
      If (RC_mode .le. 7 .and. RC_mode .ge. 5) then
         nu_RC = Min(nu_delta_pm,nu_delta_s)
      elseif (RC_mode .eq. 4) then
         nu_RC = nu_delta_pm
      elseif (RC_mode .le. 3 .and. RC_mode .ge. 1) then
         nu_RC = nu_delta_s
      Else
         Write(*,*) "RC_mode = ", RC_mode," is not allowed"
         Write(*,*) "Please put RC_mode in the range 1-7"
         Stop
      EndIf

      dpluss = nu_RC/gamdot

c     Modify dpluss to prevent the subgrain to grow larger than 
c     delta_IV (scaling).
c     And prevent stage II saturation of delta.

c     I will perhaps later change q_IV to q_cIV = q_c/q_IV

      delta_IV = (q_c/Sqrt(rhoi))/q_IV
      If(delta .ge. delta_IV) then
         dpluss= Min(-(delta_IV/delta)**13*dminus,dpluss)
      endif

c-----------------------------------
      ddelta = dminus + dpluss
c-----------------------------------

c------------------------------------------------------------------
c  -- Finally, update the variables needed by the integrator routine

      dY(1) = drhoi
      dY(2) = ddelta
      dY(3) = dphi

      Return
      End
