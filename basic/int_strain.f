      Subroutine AF_strain(rhoi, delta, phi, gamma0, gamma1, 
     &     T0, T1, gamdot0, gamdot1, D0, D1)
c
c     INTEGRATION of the microstructure one strain increment that is 
c     arbitrarily long.

c TRANSFER of variabels in the call 

c ON INPUT:   values that must be initialised:
c     rhoi    :   double precision, Interior dislocation density
c     delta   :   double precision, Subgrain size
c     phi     :   double precision, misorientation
c     gamma0  :   double precision, strain before integration
c     gamma1  :   double precision, strain after integration
c     T0      :   double precision, temperature before integration
c     T1      :   double precision, temperature after integration
c     D0      :   double precision, grainsize before integration
c     D1      :   double precision, grainsize after integration
c     gamdot0 :   double precision, strainrate before integration
c     gamdot1 :   double precision, strainrate after integration

c ON OUTPUT:    values that are calculated:
c     rhoi    :   double precision, Interior dislocation density
c     delta   :   double precision, Subgrain size
c     phi     :   double precision, misorientation

      IMPLICIT NONE
      double precision rhoi, delta, phi, 
     &     gamma0, gamma1, T0, T1, gamdot0, gamdot1, D0, D1

      external eqs_gam

c     -- integrate the microstructure (if non-zero strain step)

      If ((gamma1-gamma0).gt.0.) then
         Call Int_strain(rhoi, delta, phi, eqs_gam, 
     &        gamma0, gamma1, T0, T1, gamdot0, gamdot1, D0, D1)
      Endif
     
      return
      end

c-------------------------------------------------------------------
c-------------------------------------------------------------------
c-------------------------------------------------------------------


c-------------------------------------------------------------------
      Subroutine Int_strain(rhoi, delta, phi, equations, 
     &     gamma0, gamma1, T0, T1, gamdot0, gamdot1, D0, D1)
c-------------------------------------------------------------------
c
c INTEGRATION of the microstructure one strain increment that is 
c     arbitrarily long:

c TRANSFER of variabels in the call 

c ON INPUT:   values that must be initialised:
c     equatons:   character, name of subroutine defining the eqs syst.
c     rhoi    :   double precision, Interior dislocation density
c     delta   :   double precision, Subgrain size
c     phi     :   double precision, misorientation
c     gamma0  :   double precision, strain before integration
c     gamma1  :   double precision, strain after integration
c     T0      :   double precision, temperature before integration
c     T1      :   double precision, temperature after integration
c     D0      :   double precision, grainsize before integration
c     D1      :   double precision, grainsize after integration
c     gamdot0 :   double precision, strainrate before integration
c     gamdot1 :   double precision, strainrate after integration

c ON OUTPUT:    values that are calculated:
c     rhoi, delta, phi

      IMPLICIT NONE
      
c  -- transfered in the call
      double precision rhoi, delta, phi, 
     &     gamma0, gamma1, T0, T1, gamdot0, gamdot1, D0, D1

c  -- Local variables
      integer NEQ, LRW, LIW, NRPAR
      Parameter(NEQ=3, LRW=5+22+9*NEQ+2*NEQ**2, LIW=30+NEQ, NRPAR=8)
      Integer IWORK, itol, ipar
      DOUBLE PRECISION ATOL, RPAR, RTOL, RWORK, Y
      DIMENSION Y(NEQ), ATOL(NEQ), RWORK(LRW), IWORK(LIW),RPAR(NRPAR)
      integer itask, istate,iopt,MF,j

      EXTERNAL jac
      EXTERNAL equations

c  -- parameters to be transferred to equation system defination

      rpar(1)  = gamma0
      rpar(2)  = gamma1
      rpar(3)  = T0
      rpar(4)  = T1
      rpar(5)  = gamdot0
      rpar(6)  = gamdot1
      rpar(7)  = D0
      rpar(8)  = D1

c  -- choose method of  integration (22 = stiff, 10 =normal)
c  --    Itask = 1 normal, 5 returns after each step, 
c  --    but max range set by rwork(1)

      MF       = 10
      ITASK    = 1
      ISTATE   = 1
      IOPT     = 1
      Do j = 5,10
         Rwork(j) = 0.0
         Iwork(j) = 0
      EndDo

c  -- the length of the first step      
c     Rwork(5) = 1.0E-8
c  -- the maximal allowed steplength      
c      Rwork(6) = 1.0E-8
c  -- the minimal allowed steplength      
      Rwork(7) = 1.0E-11
c  -- the maximal strain to compute if ITASK =4 or 5
c     Rwork(1) = gamma1

c  -- integration tolerances

      RTOL = 1.D-8
      ITOL = 2
      ATOL(1) = 1.D-16
      ATOL(2) = 1.D-16
      Atol(3) = 1.0D-16
         
c  -- Integrate the micro structure gamma0 -> gamma1 
c  -- Initial conditions for the parameters to be integrated

      Y(1)      = rhoi
      Y(2)      = delta
      Y(3)      = phi

      CALL DVODE(equations,NEQ,Y,gamma0,gamma1,ITOL,RTOL,ATOL,ITASK,
     1     ISTATE,IOPT,RWORK,LRW,IWORK,LIW,jac,MF,RPAR,IPAR)
      
      rhoi      = Y(1)
      delta     = Y(2)
      phi       = Y(3)

      Return
      End






c dummy routine
c--------------------------------------------------------------------

      SUBROUTINE JAC (NEQ, T, Y, ML, MU, PD, NRPD, RPAR, IPAR)
c     Dummy subroutine for the time integrator.  The Jacobian is 
c     calculated numerical, since an analytical 
c     Jacobian is not yet implemented.
C RPAR,IPAR = user-defined real and integer arrays passed to F and JAC.
      implicit none
      integer neq, ipar, ML, MU, NRPD
      DOUBLE PRECISION PD, RPAR, T, Y
      DIMENSION Y(NEQ), PD(NRPD,NEQ)
c      PD(i,j) = df(i)/dY(j)
      RETURN
      END



c----------------------------------------------------------------------
c----------------------------------------------------------------------





