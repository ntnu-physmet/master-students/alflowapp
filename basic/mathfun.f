C
C     Some usefull mathematical functions.
C

      double precision function asinhexp(exp_term)
c     -- asinh(exp(exp_term))
c     -- arcsinh(x) = ln(x+Sqrt(x**2 +1.))
c     -- arcsinh(x) approx. ln(2)+ln(x) for x>>1
      implicit none
      double precision exp_term
      double precision arg_asinh 
      if(exp_term .gt. 50.) then
         asinhexp = log(2.) + exp_term
      else
         arg_asinh = exp(exp_term)
         asinhexp  = log(arg_asinh+Sqrt(arg_asinh**2+1.))
      endif
      return
      end

c----------------------------------------------------------
      subroutine gamfun(x,gam1, gam2)
c----------------------------------------------------------

c  -- integration of distribution functions
c  -- used to take into account subgrainsize distribtion

      implicit none
      real*8 x, gam1, gam2
      real*8 tmp
      tmp = x*x
      
      gam2 = 1. + 5.*x + 12.5*tmp
      tmp = x*tmp
      gam2 = gam2 + 20.833333333*tmp
      tmp = x*tmp
      gam2 = gam2 + 26.04166668*tmp
      tmp = x*tmp
      gam2 = gam2 + 26.04166668*tmp
      tmp = x*tmp
      gam2 = gam2 + 21.70138890*tmp
      tmp = x*tmp
      gam1 = gam2 + 15.50099207*tmp
      tmp = exp(-5.*x)
      gam2 = gam2*tmp*0.714285714
      gam1 = 1. - tmp*gam1
      return
      end


      double precision function exp2sinh(arg_exp,arg_sinh)

c     =2.*exp(arg_exp)*sinh(arg_sinh)
c     The formula 
c        arg_pre *exp(arg_exp) *2. *sinh(argsinh)
c     is exception handled in order to avoid numerical overflow:
c   * In order to better handle extreme values we use:
c        2*exp(x)*sinh(y) = exp(x+y)-exp(x-y)
c   * Exception:  y<<1 and x>>1, => inf - inf. Asymptotic:
c        exp(x)*sinh(y)  ->  exp(ln(y)+x), for y<<1.
c   * During iterations the exp() terms still
c       can become to large in the not yet converged solution.
c       Remedy: cutt-off:  exp() < exp(100.)

      implicit none
      double precision arg_exp, arg_sinh
      double precision arg1, arg2
      
      If (arg_sinh .gt. 1.E-6) then
         arg1    = arg_exp + arg_sinh
         arg2    = arg_exp - arg_sinh 
         arg1    = Min(arg1, 100.)
         arg2    = Min(arg2, 100.)
         exp2sinh = exp(arg1)-exp(arg2)
      Else
         arg1    = log(arg_sinh) + arg_exp
         arg1    = Min(arg1, 100.)
         exp2sinh = 2.0 * exp(arg1)
      EndIf
      return
      end


